﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TestJwt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        // GET: api/Token
        [HttpPost]
        public string Get([FromForm]string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return string.Empty;
            }
            return JwtHelper.CreateToken(userName);

        }
    }
}
