﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace TestJwt
{
    public static class JwtHelper
    {
        private static readonly SecurityKey _secreKey;
        private static IList<Claim> _claims;
        static JwtHelper()
        {
            _secreKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is my api please check you can use"));
        }

        private static void InitClaims(string userName)
        {
            _claims = new List<Claim>
            {
                 new Claim(JwtRegisteredClaimNames.Iss, "wei lai ke ji yan jiu suo"),
                 new Claim(ClaimTypes.Name, userName),
                 new Claim(JwtRegisteredClaimNames.Aud, "all"),
                 new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString()),
                 new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.UtcNow.AddMinutes(20)).ToUnixTimeSeconds().ToString())
            };
        }

        public static string CreateToken(string userName)
        {
            InitClaims(userName);
            var token = new JwtSecurityToken
                (new JwtHeader(new SigningCredentials
                (_secreKey, SecurityAlgorithms.HmacSha256)),
                new JwtPayload(_claims));
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
